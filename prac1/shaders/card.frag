#version 330 core

// Ouput data
out vec3 color;
in vec2 p;
flat in int t;

void main()
{
    if(t==-5) { // deactive
    	color = vec3(0.9, 0.7, 0.5);
    }else if(t==-1) { // not reversed
    	color = vec3(0.1,0.1,0.1);
    }else if(t==-2){ // selected
    	color = vec3(0.1,0.1,0.1);
    }else if(t==0) {
    	color = vec3(0.8,0,0);
    }else if(t==1) {
    	color = vec3(0,0.8,0);
    }else if(t==2) {
    	color = vec3(0,0,0.8);
    }else if(t==3) {
    	color = vec3(0.8,0.8,0);
    }else if(t==4) {
    	color = vec3(0,0.8,0.8);
    }else if(t==5) {
    	color = vec3(0.8,0,0.8);
    }else if(t==6) {
    	color = vec3(0.8,0.7,0.5);
    }else{
    	color = vec3(0.7,0.8,0.8);
    }
}
