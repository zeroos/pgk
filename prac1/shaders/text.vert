#version 330 core
 
layout(location = 0) in vec4 coord;
out vec2 texcoord;
 
void main(void) {
    gl_Position.xy = coord.xy;
    gl_Position.z = 0.0;
    gl_Position.w = 1.0;
    texcoord = coord.zw;
}
