#pragma once

#include <glm/glm.hpp>
#include <string>
#include <iostream>
#include <sstream>
#include <set>
#include <algorithm>

using namespace glm;
using namespace std;


class Card {
    public:
        vec2 pos;
        vec2 size;
        int type;
        bool reversed = false;
        bool selected = false;
        bool active = true;

        Card();
        string str();
};

class Memory {
    public:
        Card cards[16]; 
        ivec2 selected_pos;
        Card *reversed1 = NULL;
        Card *reversed2 = NULL;
        int found_pairs = 0;
        bool victory = false;
        int round = 0;

        Memory();
        void process_reversed_cards();
        bool move(ivec2 pos);
        bool move(int dx, int dy);
        bool move_right();
        bool move_left();
        bool move_up();
        bool move_down();
        bool reverse();
        static int card_pos2idx(ivec2 &pos);
};
