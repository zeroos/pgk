// Include standard headers
#include <stdio.h>
#include <stdlib.h>

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <GLFW/glfw3.h>
GLFWwindow* window;

// Include GLM
#include <glm/glm.hpp>

#include <iostream>
#include <unistd.h>
using namespace glm;
using namespace std;

#include "shader.h"
#include "game.h"
#include "text.h"

Memory memory;

static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
        if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
            glfwSetWindowShouldClose(window, GL_TRUE);
        }else if (key == GLFW_KEY_LEFT && action == GLFW_PRESS) {
            if(!memory.move_left()) {
                cout << "cannot move left" << endl;
            }
        }else if (key == GLFW_KEY_RIGHT && action == GLFW_PRESS) {
            if(!memory.move_right()) {
                cout << "cannot move right" << endl;
            }
        }else if (key == GLFW_KEY_UP && action == GLFW_PRESS) {
            if(!memory.move_up()) {
                cout << "cannot move up" << endl;
            }
        }else if (key == GLFW_KEY_DOWN && action == GLFW_PRESS) {
            if(!memory.move_down()) {
                cout << "cannot move down" << endl;
            }
        }else if (key == GLFW_KEY_SPACE && action == GLFW_PRESS) {
            if(!memory.reverse()) {
                cout << "cannot reverse this card" << endl;
            }
        }
}


int main( void )
{
	// Initialise GLFW
	if( !glfwInit() )
	{
		fprintf( stderr, "Failed to initialize GLFW\n" );
		return -1;
	}

	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	// Open a window and create its OpenGL context
	window = glfwCreateWindow(768, 768, "Memory", NULL, NULL);
	if( window == NULL ){
		fprintf( stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n" );
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);
    glfwSetKeyCallback(window, key_callback);

	// Initialize GLEW
	glewExperimental = true; // Needed for core profile
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		return -1;
	}


	// Ensure we can capture the escape key being pressed below
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

	// Dark blue background
	glClearColor(0.8f, 0.6f, 0.4f, 0.0f);

	GLuint VertexArrayID;
	glGenVertexArrays(1, &VertexArrayID);
	glBindVertexArray(VertexArrayID);

	// Create and compile our GLSL program from the shaders
	GLuint boardProgramID = LoadShaders( "shaders/board.vert", "shaders/board.frag" );
	GLuint cardProgramID = LoadShaders( "shaders/card.vert", "shaders/card.frag" );


	static const GLfloat g_vertex_board_data[] = { 
        -0.5, -1.0, -0.5, 1.0,
        -0.0, -1.0, -0.0, 1.0,
         0.5, -1.0,  0.5, 1.0,

        -1.0, -0.5, 1.0, -0.5,
        -1.0,  0.0, 1.0,  0.0,
        -1.0,  0.5, 1.0,  0.5,
	};
    GLfloat g_vertex_cards_data[12*4*4];
    GLint g_type_cards_data[12*4*4];

	GLuint vertexbuffers[3];
	glGenBuffers(3, vertexbuffers);
    cout << vertexbuffers[0] << " " << vertexbuffers[1] << endl;

	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffers[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(g_vertex_board_data), g_vertex_board_data, GL_STATIC_DRAW);


    Text text;
	do{

		// Clear the screen
		glClear( GL_COLOR_BUFFER_BIT );

        
		// Use our shader
		glUseProgram(boardProgramID);

		// 1rst attribute buffer : vertices
		glEnableVertexAttribArray(0);

		glBindBuffer(GL_ARRAY_BUFFER, vertexbuffers[0]);
		glVertexAttribPointer(
			0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
			2,                  // size
			GL_FLOAT,           // type
			GL_FALSE,           // normalized?
			0,                  // stride
			(void*)0            // array buffer offset
		);

		glDrawArrays(GL_LINES, 0, 12);

		glDisableVertexAttribArray(0);



        // draw cards
		glUseProgram(cardProgramID);
        for (int y=0; y<4; y++) {
            for(int x=0; x<4; x++) {
                int i = y*4 + x;
                Card &card = memory.cards[i];
                int j = i*12;
                g_vertex_cards_data[j] =   card.pos.x+card.size.x/2;
                g_vertex_cards_data[j+1] = card.pos.y+card.size.y/2;

                g_vertex_cards_data[j+2] = card.pos.x-card.size.x/2;
                g_vertex_cards_data[j+3] = card.pos.y+card.size.y/2;

                g_vertex_cards_data[j+4] = card.pos.x+card.size.x/2;
                g_vertex_cards_data[j+5] = card.pos.y-card.size.y/2;

                g_vertex_cards_data[j+6] = card.pos.x-card.size.x/2;
                g_vertex_cards_data[j+7] = card.pos.y-card.size.y/2;

                g_vertex_cards_data[j+8] = card.pos.x-card.size.x/2;
                g_vertex_cards_data[j+9] = card.pos.y+card.size.y/2;

                g_vertex_cards_data[j+10] = card.pos.x+card.size.x/2;
                g_vertex_cards_data[j+11] = card.pos.y-card.size.y/2;
                if(card.selected){
                    float s=0.03;
                    g_vertex_cards_data[j]   += s;
                    g_vertex_cards_data[j+1] += s;

                    g_vertex_cards_data[j+2] -= s;
                    g_vertex_cards_data[j+3] += s;

                    g_vertex_cards_data[j+4] += s;
                    g_vertex_cards_data[j+5] -= s;

                    g_vertex_cards_data[j+6] -= s;
                    g_vertex_cards_data[j+7] -= s;

                    g_vertex_cards_data[j+8] -= s;
                    g_vertex_cards_data[j+9] += s;

                    g_vertex_cards_data[j+10] += s;
                    g_vertex_cards_data[j+11] -= s;
                }

                for(int k=0; k<6; k++) {
                    if(memory.victory) {
                        g_type_cards_data[i*6+k] = rand()%10;
                    }else if(!card.active){
                        g_type_cards_data[i*6+k] = -5;
                    }else if(card.reversed){
                        g_type_cards_data[i*6+k] = card.type;
                    }else if(card.selected){
                        g_type_cards_data[i*6+k] = -2;
                    }else{
                        g_type_cards_data[i*6+k] = -1;
                    }
                }
            }
        }


        glBindBuffer(GL_ARRAY_BUFFER, vertexbuffers[1]);
        glBufferData(GL_ARRAY_BUFFER, sizeof(g_vertex_cards_data), g_vertex_cards_data, GL_STATIC_DRAW);

		// 1rst attribute buffer : vertices
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, vertexbuffers[1]);
		glVertexAttribPointer(
			0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
			2,                  // size
			GL_FLOAT,           // type
			GL_FALSE,           // normalized?
			0,                  // stride
			(void*)0            // array buffer offset
		);

        glBindBuffer(GL_ARRAY_BUFFER, vertexbuffers[2]);
        glBufferData(GL_ARRAY_BUFFER, sizeof(g_type_cards_data), g_type_cards_data, GL_STATIC_DRAW);

        glEnableVertexAttribArray(1);
		glBindBuffer(GL_ARRAY_BUFFER, vertexbuffers[2]);
		glVertexAttribPointer(
			1,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
			1,                  // size
			GL_FLOAT,           // type
			GL_FALSE,           // normalized?
			0,                  // stride
			(void*)0            // array buffer offset
		);

		glDrawArrays(GL_TRIANGLES, 0, 6*4*4);

		glDisableVertexAttribArray(1);
		glDisableVertexAttribArray(0);

        if(memory.victory) sleep(1);

        float sx = 2.0/768;
        float sy = sx;

        text.render_text(to_string(memory.round).c_str(),
                         1 - 50 * sx,   1 - 50 * sy,    sx, sy);

		// Swap buffers
		glfwSwapBuffers(window);
		glfwPollEvents();

	} // Check if the ESC key was pressed or the window was closed
	while(glfwWindowShouldClose(window) == 0 );

	// Cleanup VBO
	glDeleteBuffers(3, vertexbuffers);
	glDeleteVertexArrays(1, &VertexArrayID);
	glDeleteProgram(boardProgramID);

	// Close OpenGL window and terminate GLFW
	glfwTerminate();

	return 0;
}

