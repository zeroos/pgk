#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <ft2build.h>
#include FT_FREETYPE_H

class Text {
    public:
        FT_Library ft;
        FT_Face face;
        FT_GlyphSlot g;
        GLuint programID;

        Text();
        bool set_font(const char * font_name);
        void render_text(const char *text, float x, float y, float sx, float sy);
};


