#include "game.h"

using namespace std;
using namespace glm;

Card::Card() {
    this->size = vec2(0.4, 0.4);
}

string Card::str() {
    ostringstream out;
    out << "card(" << this->pos.x << "," << this->pos.y << ")";
    return out.str();
}

Memory::Memory() {
    srand(time(0));
    for(int y=0; y<4; y++) {
        for(int x=0; x<4; x++) {
            cards[y*4 + x].pos = vec2(x*0.5 - 0.75, y*0.5 - 0.75);
        }
    }
    set<int> s;
    for(int i=0; i<16; i++) {
        s.insert(i);
    }
    for(int i=0; i<16; i++) {
        set<int>::const_iterator it(s.begin());
        advance(it, rand()%s.size());
        cards[*it].type = i/2;
        s.erase(it);
    }

    for(int i=0; i<16; i++) {
        cout << "card " << i << " has type " << cards[i].type << endl;
    }
    cout << "game initialized" << endl;
    vec2 v = vec2(0,0);
    move(v);
}

void Memory::process_reversed_cards() {
    if(reversed1->type == reversed2->type) {
        reversed1->active = false;
        reversed2->active = false;
        found_pairs++;
        if(found_pairs == 8) {
            victory = true;
            cout << "Victory!!!" << endl;
        }
    }
    round ++;
    cout << "Round " << round << endl;
    reversed1->reversed = false;
    reversed2->reversed = false;
    reversed1 = NULL;
    reversed2 = NULL;
}

bool Memory::move(ivec2 pos) {
    // if(!cards[card_pos2idx(pos)].active) {
    //     return false;
    // }
    cards[card_pos2idx(selected_pos)].selected = false;
    cards[card_pos2idx(pos)].selected = true;
    selected_pos = pos;
    return true;
}

bool Memory::move(int dx, int dy) {
    if(reversed2) process_reversed_cards();
    for(int i=1; i<4; i++) {
        if(move(ivec2((selected_pos.x+dx*i+4)%4, (selected_pos.y+dy*i+4)%4))){
            return true;
        }
    }
    return false;
}

bool Memory::move_right() {
    return move(1, 0);
}

bool Memory::move_left() {
    return move(-1, 0);
}

bool Memory::move_up() {
    return move(0, 1);
}

bool Memory::move_down() {
    return move(0, -1);
}

bool Memory::reverse(){
    Card &c = cards[card_pos2idx(selected_pos)];
    if(!c.active || c.reversed ) {
        return false;
    }
    c.reversed = true;
    if(reversed1) {
        reversed2 = &c;
    }else{
        reversed1 = &c;
    }
    
    return true;
}

int Memory::card_pos2idx(ivec2 &pos) {
    return pos.y*4 + pos.x;
}
