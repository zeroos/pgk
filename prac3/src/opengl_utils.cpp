#include "opengl_utils.h"


namespace OpenGLUtils {
    vector<key_callback_function> key_callbacks;
    GLFWwindow* window;

    static void key_callback(GLFWwindow* window, 
        int key, int scancode, int action, int mods) {

        if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
            glfwSetWindowShouldClose(window, GL_TRUE);
        }

        for(vector<key_callback_function>::iterator i=key_callbacks.begin();
                i != key_callbacks.end(); i++) {
            (**i)(window, key, scancode, action, mods);
        }
        
    }

    int init_opengl()
    {
        // Initialise GLFW
        if( !glfwInit() )
        {
            fprintf( stderr, "Failed to initialize GLFW\n" );
            return -1;
        }

        glfwWindowHint(GLFW_SAMPLES, 4);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

        // Open a window and create its OpenGL context
        int screen_width = 768;
        int screen_height = screen_width/2 * sqrt(3);
        window = glfwCreateWindow(screen_width, screen_height, "Arkanoid", NULL, NULL);
        if( window == NULL ){
            fprintf( stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n" );
            glfwTerminate();
            return -1;
        }
        glfwMakeContextCurrent(window);

        // Initialize GLEW
        glewExperimental = true; // Needed for core profile
        if (glewInit() != GLEW_OK) {
            fprintf(stderr, "Failed to initialize GLEW\n");
            return -1;
        }


        // Ensure we can capture the escape key being pressed below
        glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

        glfwSetKeyCallback(window, key_callback);
        glClearColor(0.1f, 0.1f, 0.1f, 1.0f);

        glEnable(GL_DEPTH_TEST);



        return 0;
    }
    int terminate_opengl() {
        glfwTerminate();
        return 0;
    }
    void game_loop(void (*world_step)(double), 
                   void (*render)(double),
                   double dt) {

        double previous_time = glfwGetTime();
        double accumulator = 0.0;
	    while(glfwWindowShouldClose(window) == 0 ) {
            double time = glfwGetTime();
            double frame_time = time-previous_time;
            previous_time = time;
            
            accumulator += frame_time;

            while(accumulator >= dt) {
                (*world_step)(dt);
                accumulator -= dt;
            }
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            (*render)(accumulator/dt);
            glfwSwapBuffers(window);
            glfwPollEvents();
        }
    }
}
