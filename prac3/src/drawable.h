#pragma once

#include "camera.h"

class Drawable {
    public:
        virtual void draw(Camera &c);
};
