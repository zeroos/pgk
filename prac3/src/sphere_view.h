#pragma once

#include <vector>

#include "drawable.h"
#include "body.h"
#include "opengl_utils.h"
#include "shader.h"

class SphereView : public Drawable {
    public:
        vec3 color_diffuse;
        vec3 color_specular;
        SphereView(Body &b);
        virtual void draw(Camera &camera);
    private:
        GLuint vao;
        GLuint vbo;
        GLuint element_buffer;
        GLuint program_id;
        Body &body;
        GLint uni_translation;
        GLint uni_camera_pos;
        GLint uni_color_diffuse;
        GLint uni_color_specular;
        GLint uni_opacity;
        GLint uni_sharpness;
        GLint uni_sphere;
};
