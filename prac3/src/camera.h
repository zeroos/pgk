#pragma once

#define GLM_FORCE_RADIANS

#include <glm/glm.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtc/type_ptr.hpp>

class Camera {
    public:
        glm::mat4 projection;
        glm::mat4 view;
        glm::vec3 pos;
        glm::vec3 look_at;
        glm::vec3 up;

        Camera();
        glm::mat4 vp;
        void set_projection(glm::mat4 proj);
        void set_view(glm::mat4 view);
        void refresh_vp();
        float focal = 45.0f;
        bool follow = true;
};

