#include "camera.h"

Camera::Camera() {
    pos = glm::vec3(4, 7, 7);
    look_at = glm::vec3(0, 0, 0);
    up = glm::vec3(0, 1, 0);
    refresh_vp();
}

void Camera::refresh_vp() {
    projection = glm::perspective(focal, 4.0f / 3.0f, 0.1f, 100.0f);
    view = glm::lookAt(pos, look_at, up);
    vp = projection * view;
}
