#pragma once

#define GLM_FORCE_RADIANS

#include <glm/glm.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <iostream>
#include <random>

#include "aquarium_view.h"


extern default_random_engine generator;

class Body {
    public:
        bool destroyed;
        bool bonus;
        glm::vec3 pos;
        glm::vec3 v;
        float radius;

        Body();
        Body(glm::vec3 pos);
        virtual void world_step(float dt);
        virtual void collision(Body *other, float dt);
};

