#include "body.h"

Body::Body(glm::vec3 pos) : pos(pos), v(0,0,0), destroyed(false), radius(0) { }
Body::Body() : destroyed(false), radius(1) {
    std::uniform_real_distribution<double> w_dist(-AQUARIUM_W,   AQUARIUM_W);
    std::uniform_real_distribution<double> d_dist(-AQUARIUM_D+4, AQUARIUM_D-4);
    std::uniform_real_distribution<double> r_dist(0.001, 0.8);
    std::uniform_real_distribution<double> v_dist(0.5, 3);
    std::bernoulli_distribution bonus_dist(0.1);

    pos = vec3(
            w_dist(generator),
            -AQUARIUM_H,
            d_dist(generator)
    );
    v = vec3(0, v_dist(generator), 0);
    radius = r_dist(generator);
    if(bonus_dist(generator)) {
        bonus = true;
    }else{
        bonus = false;
    }
}

void Body::world_step(float dt) {
    pos += v*dt;
    radius += v.y*0.03*dt;
    if(pos.y > AQUARIUM_H) {
        destroyed = true;
    }
}

void Body::collision(Body *other, float dt) {
    destroyed = true;
}
