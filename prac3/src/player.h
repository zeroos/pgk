#pragma once

#include "body.h"

class Player : public Body { 
    public:
        Player(vec3 pos);
        virtual void world_step(float dt);
        bool intersects(Body *body);
        void collision(Body *other, float dt);
        void accelerate_left(bool on);
        void accelerate_right(bool on);
        void accelerate_front(bool on);
        void accelerate_back(bool on);
        void accelerate_up(bool on);
        void accelerate_down(bool on);
    private:
        typedef Body super;
        bool accelerating_left = false;
        bool accelerating_right = false;
        bool accelerating_front = false;
        bool accelerating_back = false;
        bool accelerating_up = false;
        bool accelerating_down = false;

};

