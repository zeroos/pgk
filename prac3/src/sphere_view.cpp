#include "sphere_view.h"

SphereView::SphereView(Body &b) : body(b) {
    std::uniform_real_distribution<double> dist(0, 1);

    if(body.bonus){
        color_diffuse = vec3(2.0, 2.0, 0.0);
        color_specular = vec3(1.0, 0.0, 1.0)*1.5f;
    }else{
        color_diffuse = vec3(0.5, 
                        dist(generator), 
                        1.0);
        color_specular = vec3(1.0, 1.0, 1.0)*0.5f;
    }

    float t = (1.0 + std::sqrt(5.0)) / 2.0;
    float l = std::sqrt(1+t*t);

    t/=l;
    float w =1/l;

    GLfloat vertices_initial_data[] = {
        -w,  t,  0,
         w,  t,  0,
        -w, -t,  0,   
         w, -t,  0,   

         0, -w,  t,
         0,  w,  t,
         0, -w, -t,
         0,  w, -t,

         t,  0, -w,
         t,  0,  w,
        -t,  0, -w,
        -t,  0,  w,
    };

    GLshort indices_initial_data[] = {
        0,11,5,
        0,5,1,
        0,1,7,
        0,7,10,
        0,10,11,

        1,5,9,
        5,11,4,
        11,10,2,
        10,7,6,
        7,1,8,

        3,9,4,
        3,4,2,
        3,2,6,
        3,6,8,
        3,8,9,

        4,9,5,
        2,4,11,
        6,2,10,
        8,6,7,
        9,8,1
    };

    vector<GLfloat> vertices(vertices_initial_data, vertices_initial_data + sizeof(vertices_initial_data) / sizeof(GLfloat));
    vector<GLshort> indices(indices_initial_data, indices_initial_data + sizeof(indices_initial_data) / sizeof(GLshort));

    vector<GLshort> indices2;
    /* rozmnazanie trojkacikow */
    for(int j=0; j<2; j++) {
        int s = indices.size();

        for(int i=0; i<s; i+=3) {
            vec3 v1 = vec3(vertices[indices[i]*3],
                           vertices[indices[i]*3 + 1],
                           vertices[indices[i]*3 + 2]);

            vec3 v2 = vec3(vertices[indices[i+1]*3],
                           vertices[indices[i+1]*3 + 1],
                           vertices[indices[i+1]*3 + 2]);

            vec3 v3 = vec3(vertices[indices[i+2]*3],
                           vertices[indices[i+2]*3 + 1],
                           vertices[indices[i+2]*3 + 2]);

            int m = vertices.size()/3;
            
            vec3 w1 = normalize((v1+v2)*0.5f);
            vertices.push_back(w1.x);
            vertices.push_back(w1.y);
            vertices.push_back(w1.z);

            vec3 w2 = normalize((v2+v3)*0.5f);
            vertices.push_back(w2.x);
            vertices.push_back(w2.y);
            vertices.push_back(w2.z);

            vec3 w3 = normalize((v3+v1)*0.5f);
            vertices.push_back(w3.x);
            vertices.push_back(w3.y);
            vertices.push_back(w3.z);



            indices2.push_back(indices[i]);
            indices2.push_back(m);
            indices2.push_back(m+2);


            indices2.push_back(indices[i+1]);
            indices2.push_back(m+1);
            indices2.push_back(m);


            indices2.push_back(indices[i+2]);
            indices2.push_back(m+2);
            indices2.push_back(m+1);



            indices2.push_back(m);
            indices2.push_back(m+1);
            indices2.push_back(m+2);
        }

        indices = indices2;
    }

    //cout << "s: " << indices.size() << endl;


    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, vertices.size()*sizeof(GLfloat), &vertices[0], GL_STATIC_DRAW);

    glGenBuffers(1, &element_buffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, element_buffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size()*sizeof(GLshort), &indices[0], GL_STATIC_DRAW);

    program_id = ShaderManager::load("shaders/sphere.vert", 
                                   "shaders/sphere.frag");

    ShaderManager::use_program(program_id);

    GLint posAttrib = glGetAttribLocation(program_id, "pos");
    glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(posAttrib);

    GLint normalAttrib = glGetAttribLocation(program_id, "normal");
    glVertexAttribPointer(normalAttrib, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(normalAttrib);


    uni_translation = glGetUniformLocation(program_id, "translation");
    uni_camera_pos = glGetUniformLocation(program_id, "camera_pos");
    uni_color_diffuse = glGetUniformLocation(program_id, "color_diffuse");
    uni_color_specular = glGetUniformLocation(program_id, "color_specular");
    uni_opacity = glGetUniformLocation(program_id, "opacity");
    uni_sharpness = glGetUniformLocation(program_id, "sharpness");
    uni_sphere = glGetUniformLocation(program_id, "sphere");
}

void SphereView::draw(Camera &camera) {
    if(body.destroyed) {
        return;
    }
    ShaderManager::use_program(program_id);
    glBindVertexArray(vao);

    glm::mat4 model;  // Changes for each model !
    model = glm::translate(model, body.pos);
    model = glm::scale(model, vec3(body.radius));

    glm::mat4 MVP = camera.vp * model;

    glUniformMatrix4fv(uni_translation, 1, GL_FALSE, value_ptr(MVP));
    glUniform3fv(uni_camera_pos, 1, value_ptr(camera.pos-body.pos));
    glUniform3fv(uni_color_diffuse, 1, value_ptr(color_diffuse));
    glUniform3fv(uni_color_specular, 1, value_ptr(color_specular));
    glUniform1f(uni_opacity, 1);
    glUniform1f(uni_sharpness, 50);
    glUniform1i(uni_sphere, 1);
    
    glDrawElements(GL_TRIANGLES, 1200, GL_UNSIGNED_SHORT, (void*)0);
}
