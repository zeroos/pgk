#include <iostream>
#include <list>
#include <cmath>
#include <random>
#include <algorithm>

#include "opengl_utils.h"
#include "body.h"
#include "player.h"
#include "drawable.h"
#include "sphere_view.h"
#include "aquarium_view.h"

using namespace std;

list<Drawable*> drawables;
list<Body*> movables;
list<Body*> collidables;

default_random_engine generator;
bernoulli_distribution b_dist(0.0012);

Camera camera;
Player *player;
AquariumView *aquarium_view;

int level = 1;
int points = 0;
bool game_finished = false;

void world_step(double dt) {
    if(game_finished) return;
    for(list<Body*>::iterator i=movables.begin(); i!=movables.end(); i++) {
        (*i)->world_step((float)dt);
    }

    if(camera.follow) {
        double xpos, ypos;
        glfwGetCursorPos(OpenGLUtils::window, &xpos, &ypos);

        camera.pos = player->pos;
        camera.pos.x += atan(xpos/200)*9;
        camera.pos.y += atan(ypos/200)*9;
        camera.pos.z -= 8;
        camera.look_at = player->pos;
        camera.refresh_vp();
    }else{
        camera.pos = vec3(-40,0,0);
        camera.look_at = vec3(0,0,0);
        camera.refresh_vp();
    }

    for(list<Body*>::iterator i=collidables.begin(); i!=collidables.end(); i++) {
        if((*i)->destroyed) continue;
        if(player->intersects(*i)) {
            player->collision(*i, dt);
            (*i)->collision(player, dt);
            if((*i)->bonus) {
                points += 10;
                cout << points << " points (+10)" << endl;
            }else{
                cout << "!!!GAME OVER!!!" << endl;
                cout << "You've reached level " << level
                     << " with " << points << " points." << endl;
                glfwSetWindowShouldClose(OpenGLUtils::window, GL_TRUE);
                game_finished = true;
            }
        }
    }
    if(b_dist(generator)) {
        Body *b = new Body();
        SphereView *sv = new SphereView(*b);
        drawables.push_back(sv);
        movables.push_back(b);
        collidables.push_back(b);
    }

    if(player->pos.z > AQUARIUM_D-player->radius*1.5) {
        level ++;
        points += 100;
        cout << points << " points (+100)" << endl;
        cout << "Level " << level << " reached!" << endl;
        player->pos = vec3(0,0,-AQUARIUM_D+0.5);
        b_dist = bernoulli_distribution(0.0012 * pow(1.2, (float)level));
    }
}

void render(double alpha) {
    for(list<Drawable*>::iterator i=drawables.begin(); i!=drawables.end(); i++) {
        (*i)->draw(camera);
    }
    aquarium_view->draw(camera);
}

static void key_callback(GLFWwindow* window, 
        int key, int scancode, int action, int mods) {

    if (key == GLFW_KEY_LEFT && action == GLFW_PRESS) {
        player->accelerate_left(true);
    }else if (key == GLFW_KEY_LEFT && action == GLFW_RELEASE) {
        player->accelerate_left(false);
    }else if (key == GLFW_KEY_RIGHT && action == GLFW_PRESS) {
        player->accelerate_right(true);
    }else if (key == GLFW_KEY_RIGHT && action == GLFW_RELEASE) {
        player->accelerate_right(false);
    }else if (key == GLFW_KEY_UP && action == GLFW_PRESS) {
        player->accelerate_front(true);
    }else if (key == GLFW_KEY_UP && action == GLFW_RELEASE) {
        player->accelerate_front(false);
    }else if (key == GLFW_KEY_DOWN && action == GLFW_PRESS) {
        player->accelerate_back(true);
    }else if (key == GLFW_KEY_DOWN && action == GLFW_RELEASE) {
        player->accelerate_back(false);
    }else if (key == GLFW_KEY_PAGE_UP && action == GLFW_PRESS) {
        player->accelerate_up(true);
    }else if (key == GLFW_KEY_PAGE_UP && action == GLFW_RELEASE) {
        player->accelerate_up(false);
    }else if (key == GLFW_KEY_PAGE_DOWN && action == GLFW_PRESS) {
        player->accelerate_down(true);
    }else if (key == GLFW_KEY_PAGE_DOWN && action == GLFW_RELEASE) {
        player->accelerate_down(false);
    }else if (key == GLFW_KEY_TAB && action == GLFW_PRESS) {
        camera.follow = !camera.follow;
    }
}

static void scroll_callback(GLFWwindow* window,double x,double y){
    camera.focal -= y/20;
    camera.focal = std::min(camera.focal, 46.0f);
    camera.focal = std::max(camera.focal, 44.0f);
    camera.refresh_vp();
}

int main() {
    if(OpenGLUtils::init_opengl() != 0) return -1;

    player = new Player(vec3(0,0,-AQUARIUM_D+0.5));
    aquarium_view = new AquariumView();
    SphereView pv(*player);
    pv.color_diffuse = vec3(0.2,0.8,0.2);

    drawables.push_back(&pv);
    movables.push_back(player);

    glfwSetInputMode(OpenGLUtils::window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    OpenGLUtils::key_callbacks.push_back(key_callback);
    glfwSetScrollCallback(OpenGLUtils::window, scroll_callback);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    cout << "Opengl initialized, randomizing world..." << endl;

    for(int i=0; i<10000; i++) {
        world_step(0.001);
    }
    cout << "Everything initialized, starting game loop..." << endl;

    OpenGLUtils::game_loop(world_step, render, 0.001);
    return 0;
}
