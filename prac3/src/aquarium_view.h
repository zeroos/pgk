#pragma once

#include "drawable.h"
#include "opengl_utils.h"
#include "shader.h"
#include "camera.h"

#define AQUARIUM_W 10
#define AQUARIUM_H 10
#define AQUARIUM_D 20

class AquariumView : public Drawable {
    public:
        AquariumView();
        virtual void draw(Camera &camera);
    private:
        vec3 color_diffuse;
        vec3 color_specular;
        GLuint vao;
        GLuint program_id;
        GLint uni_translation;
        GLint uni_camera_pos;
        GLint uni_color_diffuse;
        GLint uni_color_specular;
        GLint uni_opacity;
        GLint uni_sharpness;
        GLint uni_sphere;

};
