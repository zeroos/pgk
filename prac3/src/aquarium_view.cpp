#include "aquarium_view.h"

AquariumView::AquariumView(){
    float w = AQUARIUM_W;
    float h = AQUARIUM_H;
    float d = AQUARIUM_D;

    color_diffuse = vec3(0.4, 0.4, 1.0);
    color_specular = vec3(1.0, 1.0, 1.0)*0.5f;
    float vertices[] = {
        //front and back
        
        -w, -h,  d, 0, 0, -1,// Top-left
         w, -h,  d, 0, 0, -1,// Top-right
         w,  h,  d, 0, 0, -1,// Bottom-right
         w,  h,  d, 0, 0, -1,// Bottom-right
        -w,  h,  d, 0, 0, -1,// Bottom-left
        -w, -h,  d, 0, 0, -1,// Top-left

        /*-w, -h, -d, 0, 0,  1, // Top-left
         w, -h, -d, 0, 0,  1, // Top-right
         w,  h, -d, 0, 0,  1, // Bottom-right
         w,  h, -d, 0, 0,  1, // Bottom-right
        -w,  h, -d, 0, 0,  1, // Bottom-left
        -w, -h, -d, 0, 0,  1, // Top-left*/
        

        //bottom and top
        -w, -h, -d, 0,  1, 0, // Top-left
         w, -h, -d, 0,  1, 0, // Top-right
         w, -h,  d, 0,  1, 0, // Bottom-right
         w, -h,  d, 0,  1, 0, // Bottom-right
        -w, -h,  d, 0,  1, 0, // Bottom-left
        -w, -h, -d, 0,  1, 0, // Top-left

        -w,  h, -d, 0, -1, 0, // Top-left
         w,  h, -d, 0, -1, 0, // Top-right
         w,  h,  d, 0, -1, 0, // Bottom-right
         w,  h,  d, 0, -1, 0, // Bottom-right
        -w,  h,  d, 0, -1, 0, // Bottom-left
        -w,  h, -d, 0, -1, 0, // Top-left


        //side walls
         w, -h, -d, -1, 0, 0, // Top-left
         w, -h,  d, -1, 0, 0, // Top-right
         w,  h,  d, -1, 0, 0, // Bottom-right
         w,  h,  d, -1, 0, 0, // Bottom-right
         w,  h, -d, -1, 0, 0, // Bottom-left
         w, -h, -d, -1, 0, 0, // Top-left

        -w, -h, -d,  1, 0, 0, // Top-left
        -w, -h,  d,  1, 0, 0, // Top-right
        -w,  h,  d,  1, 0, 0, // Bottom-right
        -w,  h,  d,  1, 0, 0, // Bottom-right
        -w,  h, -d,  1, 0, 0, // Bottom-left
        -w, -h, -d,  1, 0, 0, // Top-left


    };

    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    GLuint vbo;
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    program_id = ShaderManager::load("shaders/sphere.vert", 
                                     "shaders/sphere.frag");

    ShaderManager::use_program(program_id);

    GLint posAttrib = glGetAttribLocation(program_id, "pos");
    glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE, sizeof(GLfloat)*6, 0);
    glEnableVertexAttribArray(posAttrib);

    GLint normalAttrib = glGetAttribLocation(program_id, "normal");
    glVertexAttribPointer(normalAttrib, 3, GL_FLOAT, GL_FALSE, 
            sizeof(GLfloat)*6, (void*)(sizeof(GLfloat)*3));
    glEnableVertexAttribArray(normalAttrib);


    uni_translation = glGetUniformLocation(program_id, "translation");
    uni_camera_pos = glGetUniformLocation(program_id, "camera_pos");
    uni_color_diffuse = glGetUniformLocation(program_id, "color_diffuse");
    uni_color_specular = glGetUniformLocation(program_id, "color_specular");
    uni_opacity = glGetUniformLocation(program_id, "opacity");
    uni_sharpness = glGetUniformLocation(program_id, "sharpness");
    uni_sphere = glGetUniformLocation(program_id, "sphere");
}

void AquariumView::draw(Camera &camera) {
    ShaderManager::use_program(program_id);
    glBindVertexArray(vao);

    glm::mat4 MVP = camera.vp;
    glUniformMatrix4fv(uni_translation, 1, GL_FALSE, value_ptr(MVP));
    glUniform3fv(uni_camera_pos, 1, value_ptr(camera.pos));
    glUniform3fv(uni_color_diffuse, 1, value_ptr(color_diffuse));
    glUniform3fv(uni_color_specular, 1, value_ptr(color_specular));
    glUniform1f(uni_opacity, 0.2);
    glUniform1f(uni_sharpness, 50);
    glUniform1i(uni_sphere, 0);

    glDrawArrays(GL_TRIANGLES, 0, 40);
}
