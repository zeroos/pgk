#include "player.h"

void Player::world_step(float dt) {
    float acceleration = 7;

    if(accelerating_left) {
        v.x += dt*acceleration;
    }else if(accelerating_right) {
        v.x -= dt*acceleration;
    }else if(accelerating_front) {
        v.z += dt*acceleration;
    }else if(accelerating_back) {
        v.z -= dt*acceleration;
    }else if(accelerating_up) {
        v.y += dt*acceleration;
    }else if(accelerating_down) {
        v.y -= dt*acceleration;
    }
    v *= dt*1000*0.999;

    if(pos.x-radius < -AQUARIUM_W) {
        pos.x = -AQUARIUM_W+radius;
        v.x = 0;
    }else if(pos.x+radius > AQUARIUM_W) {
        pos.x = AQUARIUM_W-radius;
        v.x = 0;
    }

    if(pos.y-radius < -AQUARIUM_H) {
        pos.y = -AQUARIUM_H+radius;
        v.y = 0;
    }else if(pos.y+radius > AQUARIUM_H) {
        pos.y = AQUARIUM_H-radius;
        v.y = 0;
    }

    if(pos.z-radius < -AQUARIUM_D) {
        pos.z = -AQUARIUM_D+radius;
        v.z = 0;
    }else if(pos.z+radius > AQUARIUM_D) {
        pos.z = AQUARIUM_D-radius;
        v.z = 0;
    }



    pos += v*dt;
}

void Player::accelerate_left(bool on) {
    accelerating_left = on;
}

void Player::accelerate_right(bool on) {
    accelerating_right = on;
}

void Player::accelerate_front(bool on) {
    accelerating_front = on;
}

void Player::accelerate_back(bool on) {
    accelerating_back = on;
}

void Player::accelerate_up(bool on) {
    accelerating_up = on;
}

void Player::accelerate_down(bool on) {
    accelerating_down = on;
}

bool Player::intersects(Body *body) {
    float f = glm::distance(body->pos, pos);
    return (f < body->radius + radius);
}

void Player::collision(Body *other, float dt) {
    //pass
}

Player::Player(vec3 pos) : Body(pos) {
    radius = 1;
}
