#version 330 core

layout(location = 0) in vec3 pos;

uniform vec3 camera_pos;
uniform mat4 translation;

out vec3 camera_distance;

void main(){
    gl_Position = translation*vec4(pos, 1.0);
    camera_distance = pos-camera_pos;
}

