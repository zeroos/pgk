#version 330 core

// Ouput data


in vec3 camera_distance;

out vec4 color;

void main()
{
    float d = length(camera_distance);
    vec3 glass_c = vec3(0.4, 0.4, 1);
    //vec3 water_c = vec3(0.24, 0, 1.0);
    vec3 water_c = vec3(0.2, 0.2, 1.0);
	color = vec4((glass_c + water_c * d) * 10/pow(d,2), 0.5);
}
