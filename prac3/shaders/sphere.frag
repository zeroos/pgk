#version 330 core

// Ouput data
out vec4 color;
in vec3 N;
in vec3 camera_light;

uniform vec3 color_diffuse;
uniform vec3 color_specular;
uniform float opacity;
uniform float sharpness;

void main()
{
    vec3 sun = vec3(0.0, 1.0, 0.0);

    vec3 Rsun = 2*dot(normalize(sun), normalize(N))*normalize(N) - normalize(sun);
    vec3 Rcamera = 2*dot(normalize(-camera_light), normalize(N))*normalize(N) - normalize(-camera_light);

    vec3 camera_light_color = vec3(1.0, 1.0, 0.5);
    vec3 sun_light_color = vec3(1.0, 1.0, 1.0);
    
    vec3 c = vec3(0,0,0);
    c += color_diffuse  * sun_light_color    * max(dot(N, sun), 0);
    c += color_diffuse  * camera_light_color * max(dot(N, normalize(-camera_light)), 0);
    c += color_specular * sun_light_color    * max(pow(dot(normalize(Rsun), normalize(-camera_light)), sharpness), 0);
    c += color_specular * camera_light_color * max(pow(dot(normalize(Rcamera), normalize(-camera_light)), sharpness), 0);
    c += 0.5 * color_diffuse; // emissive

    float d = length(camera_light);
    if(camera_light.x > 10) {
        d -= camera_light.x - 10;
    }
    vec3 water_c = vec3(0.2, 0.2, 1.0);
    c -= ((vec3(1,1,1)-water_c)*d/20);
    c *= 80/pow(d,2);

    color = vec4(c, opacity);
}
