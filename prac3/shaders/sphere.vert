#version 330 core

layout(location = 0) in vec3 pos;
layout(location = 1) in vec3 normal;

out vec3 N;
out vec3 camera_light;

uniform vec3 camera_pos;
uniform vec3 color_diffuse;
uniform vec3 color_specular;
uniform mat4 translation;
uniform bool sphere;

void main(){
    N = normal;
    gl_Position = translation*vec4(pos, 1.0);
    camera_light =  -camera_pos+pos;
}

