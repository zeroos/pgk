#version 330 core

// Ouput data
out vec3 color;
flat in int outline_c;
flat in float color_s;

void main()
{
    if(outline_c == 1) {
    	color = vec3(0.6,0.6,0.6);
    }else{
    	color = vec3(0.2,0.2,color_s);
    }

}
