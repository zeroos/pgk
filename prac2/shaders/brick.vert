#version 330 core

layout(location = 0) in vec2 pos;

uniform mat4 translation;
uniform int outline;
uniform float color;

flat out int outline_c;
flat out float color_s;

void main(){
    outline_c = outline;
    color_s = color;
    if(outline == 1) {
        gl_Position = translation*vec4(pos, 0.0, 1.0);
    }else{
        gl_Position = translation*vec4(pos.x*0.9, pos.y*0.85, 0.0, 1.0);
    }
}

