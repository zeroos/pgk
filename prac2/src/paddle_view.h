#pragma once

#include "drawable.h"
#include "paddle.h"
#include "opengl_utils.h"
#include "shader.h"

class PaddleView : public Drawable {
    public:
        PaddleView(Paddle &b);
        virtual void draw();
    private:
        GLuint vao;
        GLuint program_id;
        Paddle &paddle;
        GLint uni_translation;
};
