#pragma once

#include <map>
#include <cstring>


namespace ShaderManager {
    struct char_cmp {
        bool operator () (const char *a,const char *b) const {
            return strcmp(a,b)<0;
        } 
    };
    typedef std::map<const char *, int, char_cmp> Map;
    
    extern Map programs;
    extern GLuint current_program_id;

    GLuint load(const char * vertex_file_path, const char * fragment_file_path);
    void use_program(GLuint program_id);
}

