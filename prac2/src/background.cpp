#include "background.h"

Background::Background() {
    float center_x = 0;
    float center_y = 0;
    float size = 0.1;
    float h_width = size*2;
    float h_height = sqrt(3)/2 * h_width;

    int x_num = 2/(h_width*3/4)+1;
    int y_num = 2/(h_height)+1;
    hex_num = x_num*y_num;
    float vertices[hex_num*2*12];

    int i=0;
    for(float y=-1; y<=1; y+=h_height){
        bool even = true;
        for(float x=-1; x<=1; x+=h_width*3/4){
            for(int j=0; j<=6; j++) {
                float angle = 2 * M_PI / 6 * (j);
                float x_i = x + size * cos(angle);
                float y_i = y + size * sin(angle);
                if(!even) y_i += h_height/2;
                vertices[i++] = x_i;
                vertices[i++] = y_i;
                if(j!=0 && j!=6){
                    vertices[i++] = x_i;
                    vertices[i++] = y_i;
                }
            }
            even = !even;
        }
    }

    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    GLuint vbo;
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    program_id = ShaderManager::load("shaders/background.vert", 
                                   "shaders/background.frag");

    ShaderManager::use_program(program_id);

    GLint posAttrib = glGetAttribLocation(program_id, "pos");
    glVertexAttribPointer(posAttrib, 2, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(posAttrib);
}

void Background::draw() {
    ShaderManager::use_program(program_id);
    glBindVertexArray(vao);
    glDrawArrays(GL_LINES, 0, hex_num*12);
}
