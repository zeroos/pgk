#pragma once

#include "drawable.h"
#include "ball.h"
#include "opengl_utils.h"
#include "shader.h"

class BallView : public Drawable {
    public:
        BallView(Ball &b);
        virtual void draw();
    private:
        GLuint vao;
        GLuint program_id;
        Ball &ball;
        GLint uni_translation;
};
