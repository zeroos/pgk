#include "body.h"

Body::Body(float x, float y, float r) : pos(x, y), r(r), v(0,0), destroyed(false){ }
Body::Body(glm::vec2 pos, float r) : pos(pos), r(r), v(0,0), destroyed(false) { }
Body::Body() : pos(0, 0), r(0), v(0,0), destroyed(false) { }

void Body::world_step(float dt) {
    pos += v*dt;
}

void Body::collision(Body *other, float dt) {
    //pass
}
