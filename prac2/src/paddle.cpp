#include "paddle.h"

using namespace glm;

Paddle::Paddle() {
    size = vec2(0.07, 0.01);
    pos = vec2(0, -0.95);
    v = vec2(0, 0);
}

void Paddle::accelerate_left(bool on) {
    accelerating_left = on;
}

void Paddle::accelerate_right(bool on) {
    accelerating_right = on;
}

void Paddle::world_step(float dt) {
    if(accelerating_left){
        v.x -= dt;
    }
    if(accelerating_right){
        v.x += dt;
    }
    v.x *= 0.999;
    if(!accelerating_right && !accelerating_left && abs(v.x) < 0.0001){
        v.x = 0;
    }

    if((pos.x <= -0.38 && v.x < 0)  ||   
       (pos.x >= 0.38 && v.x > 0)) {
        v.x = 0;
    }

    super::world_step(dt);
}

void Paddle::collision(Body *other, float dt) {
    other->v += this->v*0.5f;
}
