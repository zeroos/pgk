#pragma once

#include <cmath>

#include "drawable.h"
#include "opengl_utils.h"
#include "shader.h"

class Background : public Drawable {
    public:
        Background();
        void draw();
    private:
        GLuint vao;
        GLuint program_id;
        int hex_num;
};
