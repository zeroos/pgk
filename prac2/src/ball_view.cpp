#include "ball_view.h"

BallView::BallView(Ball &b) : ball(b) {
    float vertices[14];
    int i = 0;
    for(int j=0; j<7; j++) {
        float angle = 2 * M_PI / 6 * (j);
        float x_i = ball.radius * cos(angle);
        float y_i = ball.radius * sin(angle);
        vertices[i++] = x_i;
        vertices[i++] = y_i;
    }

    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    GLuint vbo;
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    program_id = ShaderManager::load("shaders/ball.vert", 
                                   "shaders/ball.frag");

    ShaderManager::use_program(program_id);

    GLint posAttrib = glGetAttribLocation(program_id, "pos");
    glVertexAttribPointer(posAttrib, 2, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(posAttrib);
}

void BallView::draw() {
    ShaderManager::use_program(program_id);
    glBindVertexArray(vao);

    mat4 translation;
    translation = glm::translate(translation, vec3(ball.pos.x, ball.pos.y,0));
    translation = glm::rotate(translation, ball.r, vec3(0,0,1));

    uni_translation = glGetUniformLocation(program_id, "translation");
    glUniformMatrix4fv(uni_translation, 1, GL_FALSE, value_ptr(translation));
    
    glDrawArrays(GL_TRIANGLE_FAN, 0, 7);
}
