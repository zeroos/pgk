#include <iostream>
#include <vector>

#include "opengl_utils.h"
#include "background.h"
#include "wall.h"
#include "ball.h"
#include "paddle.h"
#include "brick.h"
#include "wall_view.h"
#include "ball_view.h"
#include "paddle_view.h"
#include "brick_view.h"

using namespace std;

Ball *ball;
Paddle *paddle;

vector<Drawable*> drawables;
vector<Body*> movables;
vector<Body*> collidables;


void world_step(double dt) {
    for(vector<Body*>::iterator i=movables.begin(); i!=movables.end(); i++) {
        (*i)->world_step((float)dt);
    }

    for(vector<Body*>::iterator i=collidables.begin(); i!=collidables.end(); i++) {
        if((*i)->destroyed) continue;
        if(ball->intersects(*i)) {
            ball->collision(*i, dt);
            (*i)->collision(ball, dt);
        }
    }
}

void render(double alpha) {
    for(vector<Drawable*>::iterator i=drawables.begin(); i!=drawables.end(); i++) {
        (*i)->draw();
    }
}

static void key_callback(GLFWwindow* window, 
        int key, int scancode, int action, int mods) {

    if (key == GLFW_KEY_LEFT && action == GLFW_PRESS) {
        paddle->accelerate_left(true);
    }else if (key == GLFW_KEY_LEFT && action == GLFW_RELEASE) {
        paddle->accelerate_left(false);
    }else if (key == GLFW_KEY_RIGHT && action == GLFW_PRESS) {
        paddle->accelerate_right(true);
    }else if (key == GLFW_KEY_RIGHT && action == GLFW_RELEASE) {
        paddle->accelerate_right(false);
    }
}

int main() {
    if(OpenGLUtils::init_opengl() != 0) return -1;

    Background *background = new Background();
    ball = new Ball();
    paddle = new Paddle();

    movables.push_back(ball);
    movables.push_back(paddle);

    drawables.push_back(background);
    for(int i=0; i<6; i++) {
        Wall *w = new Wall(i);
        drawables.push_back(new WallView(*w));
        collidables.push_back(w);
    }

    float y_b = 0.6;
    float y_d = 0.04;
    for(float y=0.05; y<y_b; y+=y_d) {
        float x_b = 0.6;
        float x_d = 0.06;
        for(float x=-x_b; x<x_b; x+=x_d) {
            Brick *b = new Brick(vec2(x,y));
            drawables.push_back(new BrickView(*b));
            collidables.push_back(b);
        }
    }

    drawables.push_back(new BallView(*ball));
    collidables.push_back(paddle);
    drawables.push_back(new PaddleView(*paddle));

    OpenGLUtils::key_callbacks.push_back(key_callback);
    OpenGLUtils::game_loop(world_step, render, 0.001);
    return 0;
}
