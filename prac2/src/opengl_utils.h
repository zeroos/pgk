#pragma once

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <GLFW/glfw3.h>

// Include GLM

#define GLM_FORCE_RADIANS

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <iostream>
#include <unistd.h>
#include <vector>

using namespace glm;
using namespace std;

namespace OpenGLUtils {
    typedef void (*key_callback_function)(GLFWwindow* window,
                int key, int scancode, int action, int mods);

    extern vector<key_callback_function> key_callbacks;

    int init_opengl();
    int terminate_opengl();
    void game_loop(void (*world_step)(double), 
                   void (*render)(double),
                   double dt);
}

