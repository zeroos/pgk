#pragma once

#include "drawable.h"
#include "wall.h"
#include "opengl_utils.h"
#include "shader.h"

class WallView : public Drawable {
    public:
        WallView(Wall &w);
        virtual void draw();
    private:
        GLuint vao;
        GLuint program_id;
        Wall &wall;
        GLint uni_translation;
};
