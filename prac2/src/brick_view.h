#pragma once

#include "drawable.h"
#include "brick.h"
#include "opengl_utils.h"
#include "shader.h"

class BrickView : public Drawable {
    public:
        BrickView(Brick &w);
        virtual void draw();
    private:
        GLuint vao;
        GLuint program_id;
        Brick &brick;
        GLint uni_translation;
};
