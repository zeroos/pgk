#pragma once

#include "body.h"


class Brick : public Body {
    public:
        Brick(glm::vec2 pos);
        virtual void collision(Body *other, float dt);
};
