#include "brick.h"

using namespace glm;

Brick::Brick(glm::vec2 pos) {
    this->pos = pos;
    size = vec2(0.03, 0.02);
}

void Brick::collision(Body *other, float dt) {
    destroyed = true;
}
