#include "wall_view.h"

WallView::WallView(Wall &w) : wall(w) {
    float &x=wall.size.x;
    float &y=wall.size.y;
    float vertices[] = {
        -x,  y, // Top-left
        x,  y,  // Top-right
        x, -y,  // Bottom-right

        x, -y,  // Bottom-right
        -x, -y, // Bottom-left
        -x,  y, // Top-left
    };

    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    GLuint vbo;
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    program_id = ShaderManager::load("shaders/wall.vert", 
                                   "shaders/wall.frag");

    ShaderManager::use_program(program_id);

    GLint posAttrib = glGetAttribLocation(program_id, "pos");
    glVertexAttribPointer(posAttrib, 2, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(posAttrib);
}

void WallView::draw() {
    ShaderManager::use_program(program_id);
    glBindVertexArray(vao);

    mat4 translation;
    translation = glm::translate(translation, vec3(wall.pos.x,wall.pos.y,0));
    translation = glm::rotate(translation, wall.r, vec3(0,0,1));

    uni_translation = glGetUniformLocation(program_id, "translation");
    glUniformMatrix4fv(uni_translation, 1, GL_FALSE, value_ptr(translation));
    
    glDrawArrays(GL_TRIANGLES, 0, 6);
}
