#include "brick_view.h"

BrickView::BrickView(Brick &w) : brick(w) {
    float &x=brick.size.x;
    float &y=brick.size.y;
    float vertices[] = {
        -x,  y, // Top-left
        x,  y,  // Top-right
        x, -y,  // Bottom-right

        x, -y,  // Bottom-right
        -x, -y, // Bottom-left
        -x,  y, // Top-left
    };

    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    GLuint vbo;
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    program_id = ShaderManager::load("shaders/brick.vert", 
                                   "shaders/brick.frag");

    ShaderManager::use_program(program_id);

    GLint posAttrib = glGetAttribLocation(program_id, "pos");
    glVertexAttribPointer(posAttrib, 2, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(posAttrib);
}

void BrickView::draw() {
    if(brick.destroyed) return;
    ShaderManager::use_program(program_id);
    glBindVertexArray(vao);

    mat4 translation;
    translation = glm::translate(translation, vec3(brick.pos.x,brick.pos.y,0));
    translation = glm::rotate(translation, brick.r, vec3(0,0,1));

    uni_translation = glGetUniformLocation(program_id, "translation");
    glUniformMatrix4fv(uni_translation, 1, GL_FALSE, value_ptr(translation));


    GLuint uni_outline = glGetUniformLocation(program_id, "outline");
    glUniform1i(uni_outline, 1);


    GLuint uni_color = glGetUniformLocation(program_id, "color");
    glUniform1f(uni_color, brick.pos.y+0.2);
    
    glDrawArrays(GL_TRIANGLES, 0, 6);
    glUniform1i(uni_outline, 0);
    glDrawArrays(GL_TRIANGLES, 0, 6);
}
