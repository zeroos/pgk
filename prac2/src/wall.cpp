#include "wall.h"

using namespace glm;

Wall::Wall(int num) {
    all_penalty_points = 0;
    penalty_points = 0;

    size = vec2(1.0, 0.5);
    if(num==0){
        pos = vec2(-1, -1);
        r = -M_PI/3;
    }else if(num==1){
        pos = vec2(-1, 1);
        r = M_PI/3;
    }else if(num==2){
        pos = vec2(1, 1);
        r = -M_PI/3;
    }else if(num==3){
        pos = vec2(1, -1);
        r = M_PI/3;
    }else if(num==4){
        pos = vec2(0, -1.49);
        r = 0;
        penalty_points = 1;
    }else if(num==5){
        pos = vec2(0, 1.49);
        r = 0;
    }

}

void Wall::collision(Body *other, float dt) {
    if(penalty_points != 0){
        all_penalty_points -= penalty_points;
        std::cout << all_penalty_points << " points" << std::endl;
    }
}
