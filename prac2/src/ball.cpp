#include "ball.h"

using namespace glm;

Ball::Ball() {
    radius = 0.01;
    pos = vec2(0, 0);
    v = vec2(-0.1, -0.5);
}

bool Ball::intersects(Body *body) {
    vec2 p = glm::rotate(pos-body->pos, -body->r);
    if( abs(p.x) > body->size.x || 
        abs(p.y) > body->size.y) {
        return false;
    }
    return true;
}

void Ball::collision(Body *other, float dt) {
    vec2 v2;

    pos -= v*dt;
    v2 = glm::rotate(v, -other->r);
    v2.y = -v2.y;
    v2 = glm::rotate(v2, other->r);
    pos += v2*dt;
    if(!this->intersects(other)){
        v = v2;
        return;
    }

    pos -= v2*dt;
    v2 = glm::rotate(v, -other->r);
    v2.x = -v2.x;
    v2 = glm::rotate(v2, other->r);
    pos += v2*dt;
    v = v2;
    if(this->intersects(other)){
        std::cout << "cannot figure out collision..." << std::endl;
        return;
    }
}
