#pragma once

#include "body.h"

class Ball : public Body {
    public:
        float radius;

        Ball();
        bool intersects(Body *body);
        void collision(Body *other, float dt);
};
