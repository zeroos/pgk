#pragma once

#include "body.h"

class Paddle : public Body {
    public:
        float width;

        Paddle();
        void accelerate_right(bool on);
        void accelerate_left(bool on);
        virtual void world_step(float dt);
        virtual void collision(Body *other, float dt);
    private:
        typedef Body super;
        bool accelerating_right = false;
        bool accelerating_left = false;
};
