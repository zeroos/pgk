#pragma once

#include "body.h"


class Wall : public Body {
    public:
        Wall(int num);
        virtual void collision(Body *other, float dt);
    private:
        int penalty_points;
        int all_penalty_points;
};
