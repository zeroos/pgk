#pragma once

#define GLM_FORCE_RADIANS

#include <glm/glm.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <iostream>


class Body {
    public:
        bool destroyed;
        glm::vec2 pos;
        glm::vec2 size = glm::vec2(0,0);
        glm::vec2 v;
        float r;

        Body();
        Body(glm::vec2 pos, float r);
        Body(float x, float y, float r);
        virtual void world_step(float dt);
        virtual void collision(Body *other, float dt);
};

