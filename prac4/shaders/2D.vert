#version 330 core

layout(location = 0) in vec2 pos;
layout(location = 1) in float h;

out float ht;

uniform mat4 translation;

void main(){
    gl_Position = translation*vec4(pos, 0.0, 1.0);
    ht = h;
}

