#version 330 core

// Ouput data
out vec3 color;

in float ht;

void main()
{
    float htp = (ht-6371)/1.3 *2000;

    if      (htp < 0  )   color = vec3(0.,       0.,        1.); //blue
    else if (htp < 500)   color = vec3(0.,       ht/500,    0.); //->green
    else if (htp < 1000)  color = vec3(ht/500-1, 1.,        0.); //->yellow
    else if (htp < 2000)  color = vec3(1.,       2.-ht/1000,0.); //->red
    else                 color = vec3(1.,1.,1.);                //white

}
