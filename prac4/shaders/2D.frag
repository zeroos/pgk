#version 330 core

// Ouput data
out vec3 color;

in float ht;

void main()
{
    if      (ht < 0  )   color = vec3(0.,       0.,        1.); //blue
    else if (ht < 500)   color = vec3(0.,       ht/500,    0.); //->green
    else if (ht < 1000)  color = vec3(ht/500-1, 1.,        0.); //->yellow
    else if (ht < 2000)  color = vec3(1.,       2.-ht/1000,0.); //->red
    else                 color = vec3(1.,1.,1.);                //white
}
