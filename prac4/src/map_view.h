#pragma once

#include <vector>

#include "drawable.h"
#include "body.h"
#include "opengl_utils.h"
#include "shader.h"
#include "srtm.h"

class MapView : public Drawable {
    public:
        MapView(SRTM &s);
        virtual void draw(Camera &camera);
        void set_lod(int lod, bool view_3D);
        glm::vec3 center;
    private:
        bool view_3D = false;
        GLuint vao;
        GLuint vbo;
        GLuint element_buffer;
        GLuint program_3d_id;
        GLuint program_2d_id;
        SRTM &srtm;
        GLint uni_translation;
        int w,h;
};
