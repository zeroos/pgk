#pragma once

#include <iostream>
#include <fstream>

#include "opengl_utils.h"

using namespace std;

class SRTM {
    public:
        GLfloat * data;
        int w, h;
        float w_deg, h_deg;
        int size;
        float sample_deg = 3.0/3600;
        float lat;
        float lng;
        SRTM(string filename);
        ~SRTM();
};
