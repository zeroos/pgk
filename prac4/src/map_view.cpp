#include "map_view.h"

MapView::MapView(SRTM &s) : srtm(s) {
}

void MapView::set_lod(int lod, bool view_3D) {
    this->view_3D = view_3D;
    w = 17 + (lod-1)*148;
    h = w;


    vector<GLfloat> heights;
    heights.resize(w*h);
    for(int y=0; y<h; y++) { 
        for(int x=0; x<w; x++) {
            int xp, yp;
            if(x==0){
                xp = 0;
            }else{
                xp = x * srtm.w / w;
            }

            if(y==0){
                yp = 0;
            }else{
                yp = y * srtm.h / h;
            }
            heights[(y*w+x)] = srtm.data[yp*srtm.w+xp];
            //cout << heights[(y*w+x)] << endl;
        }
    }

    vector<GLfloat> vertices;
    vertices.resize(w*h*2);
    float minz = 100000;
    float maxz = -100000;
    for(int y=0; y<h; y++) {
        for(int x=0; x<w; x++) {
            if(view_3D) {
                float r = (6371000 + heights[(y*w+x)])/1000;
                vertices[(y*w+x)*2]     = srtm.lng +   ((float)x)/(w-1) * srtm.w_deg;
                vertices[(y*w+x)*2 + 1] = srtm.lat +   ((float)(h-y))/(h-1) * srtm.h_deg;

                glm::vec3 v = glm::vec3(r, 0, 0);
                v = glm::rotateZ(v, (float)(vertices[(y*w+x)*2+1] * M_PIl / 180));
                v = glm::rotateY(v, (float)(vertices[(y*w+x)*2]   * M_PIl / 180));

                vertices[(y*w+x)*2] = v.x;
                vertices[(y*w+x)*2+1] = v.y;
                heights[(y*w+x)] = v.z;
                center = v;
                //cout << v.x << " " << v.y << " " << v.z << endl;
                v.z = glm::length(v);
                if(v.z < minz) minz = v.z;
                if(v.z > maxz) maxz = v.z;

            }else{
                vertices[(y*w+x)*2]     = srtm.lng +   ((float)x)/(w-1) * srtm.w_deg;
                vertices[(y*w+x)*2 + 1] = srtm.lat +   ((float)(h-y))/(h-1)     * srtm.h_deg;

                vertices[(y*w+x)*2 + 1] *= 1/cos(vertices[(y*w+x)*2 + 1] * M_PIl / 180);

                center = glm::vec3(vertices[(y*w+x)*2], vertices[(y*w+x)*2+1], 0);
            }
        }
    } 
    //cout << "MINZ: " << minz << endl;
    //cout << "MAXZ: " << maxz << endl;

     
    vector<GLuint> indices;
    indices.resize((w-1)*(h-1)*2*3);
    for(GLuint y=0; y<h-1; y++) {
        for(GLuint x=0; x<w-1; x++) {
           indices[(y*(w-1)+x)*6] = y*w+x;
           indices[(y*(w-1)+x)*6 + 1] = y*w+(x+1);
           indices[(y*(w-1)+x)*6 + 2] = (y+1)*w+(x+1);

           indices[(y*(w-1)+x)*6 + 3] = y*w+x;
           indices[(y*(w-1)+x)*6 + 4] = (y+1)*w+(x+1);
           indices[(y*(w-1)+x)*6 + 5] = (y+1)*w+x;
        }
    }

    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, vertices.size()*sizeof(GLfloat), &vertices[0], GL_STATIC_DRAW);

    glGenBuffers(1, &element_buffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, element_buffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, w*h*2*3*sizeof(GLuint), &indices[0], GL_STATIC_DRAW);

    program_2d_id = ShaderManager::load("shaders/2D.vert", 
                                        "shaders/2D.frag");

    program_3d_id = ShaderManager::load("shaders/3D.vert", 
                                        "shaders/3D.frag");

    //ShaderManager::use_program(program_id);

    GLint posAttrib = glGetAttribLocation(program_2d_id, "pos");
    glVertexAttribPointer(posAttrib, 2, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(posAttrib);

    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, heights.size()*sizeof(GLfloat), &heights[0], GL_STATIC_DRAW);

    GLint hAttrib = glGetAttribLocation(program_2d_id, "h");
    glVertexAttribPointer(hAttrib, 1, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(hAttrib);

    uni_translation = glGetUniformLocation(program_2d_id, "translation");
}

void MapView::draw(Camera &camera) {
    if(view_3D) {
        ShaderManager::use_program(program_3d_id);
    }else{
        ShaderManager::use_program(program_2d_id);
    }
    glBindVertexArray(vao);

    glm::mat4 model;  // Changes for each model !
    glm::mat4 MVP = camera.vp * model;

    glUniformMatrix4fv(uni_translation, 1, GL_FALSE, value_ptr(MVP));
    
    glDrawElements(GL_TRIANGLES, (w-1)*(h-1)*6, GL_UNSIGNED_INT, (void*)0);
}
