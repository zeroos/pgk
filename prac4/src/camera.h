#pragma once

#define GLM_FORCE_RADIANS

#include <glm/glm.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "body.h"

class Camera : public Body {
    public:
        glm::mat4 projection;
        glm::mat4 view;
        glm::vec3 pos;
        glm::vec3 velocity;
        glm::vec3 look_at;
        glm::vec3 up;
        bool accelerating_left, accelerating_right;
        bool accelerating_front, accelerating_back;
        bool accelerating_up, accelerating_down;
        bool perspective = false;

        Camera();

        glm::mat4 vp;
        void set_projection(glm::mat4 proj);
        void set_view(glm::mat4 view);
        void refresh_vp();
        void accelerate_left(bool a);
        void accelerate_right(bool a);
        void accelerate_up(bool a);
        void accelerate_down(bool a);
        void accelerate_front(bool a);
        void accelerate_back(bool a);
        void world_step(float dt);
        float focal = 45.0f;
};

