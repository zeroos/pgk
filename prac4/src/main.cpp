#include <iostream>
#include <list>
#include <cmath>
#include <random>
#include <algorithm>

#include "opengl_utils.h"
#include "body.h"
#include "drawable.h"
#include "srtm.h"
#include "map_view.h"

using namespace std;

list<Drawable*> drawables;
list<Body*> movables;
list<MapView*> map_views;

Camera camera;
int current_lod = 2;
bool auto_lod = false;
double last_lod_adj_time = 0;
bool view_3D = false;

void set_lod(int lod) {
    cout << "LOD level: " << lod << endl;
    current_lod = lod;
    for(list<MapView*>::iterator i=map_views.begin(); i!=map_views.end(); i++) {
        (*i)->set_lod(lod, view_3D);
    }
}

void world_step(double dt) {

    if(view_3D) {
        double xpos, ypos;
        glfwGetCursorPos(OpenGLUtils::window, &xpos, &ypos);
        glm::vec3 v = glm::vec3(0,0,-1);
        glm::vec3 u = glm::vec3(0,1,0);
        v = glm::rotateY(v, (float)-xpos/1000);
        v = glm::rotateX(v, (float)-ypos/1000);
        u = glm::rotateY(u, (float)-xpos/1000);
        u = glm::rotateX(u, (float)-ypos/1000);
        camera.look_at = v;
        camera.up = u;
        camera.perspective = true;
    }else{
        glfwSetCursorPos(OpenGLUtils::window, 0, 0);
        camera.look_at = glm::vec3(0,0,-1);
        camera.up = glm::vec3(0,1,0);
        camera.perspective = false;
    }



    for(list<Body*>::iterator i=movables.begin(); i!=movables.end(); i++) {
        (*i)->world_step((float)dt);
    }

    if(auto_lod && glfwGetTime() - last_lod_adj_time > 1){
        if(OpenGLUtils::fps < 30 && current_lod > 1) {
            cout << "auto LOD too high" << endl;
            set_lod(current_lod - 1);
        }else if(OpenGLUtils::fps > 50 && current_lod < 9) {
            cout << "auto LOD too low" << endl;
            set_lod(current_lod + 1);
        }
        last_lod_adj_time = glfwGetTime();
    }
    
    /*for(list<Body*>::iterator i=collidables.begin(); i!=collidables.end(); i++) {
        if((*i)->destroyed) continue;
        if(player->intersects(*i)) {
            player->collision(*i, dt);
            (*i)->collision(player, dt);
        }
    }*/
}

void render(double alpha) {
    for(list<Drawable*>::iterator i=drawables.begin(); i!=drawables.end(); i++) {
        (*i)->draw(camera);
    }
}

static void key_callback(GLFWwindow* window, 
        int key, int scancode, int action, int mods) {

    if (key == GLFW_KEY_LEFT && action == GLFW_PRESS) {
        camera.accelerate_left(true);
    }else if (key == GLFW_KEY_LEFT && action == GLFW_RELEASE) {
        camera.accelerate_left(false);
    }else if (key == GLFW_KEY_RIGHT && action == GLFW_PRESS) {
        camera.accelerate_right(true);
    }else if (key == GLFW_KEY_RIGHT && action == GLFW_RELEASE) {
        camera.accelerate_right(false);
    }else if (key == GLFW_KEY_UP && action == GLFW_PRESS) {
        camera.accelerate_up(true);
    }else if (key == GLFW_KEY_UP && action == GLFW_RELEASE) {
        camera.accelerate_up(false);
    }else if (key == GLFW_KEY_DOWN && action == GLFW_PRESS) {
        camera.accelerate_down(true);
    }else if (key == GLFW_KEY_DOWN && action == GLFW_RELEASE) {
        camera.accelerate_down(false);
    }else if (key == GLFW_KEY_PAGE_UP && action == GLFW_PRESS) {
        camera.accelerate_front(true);
    }else if (key == GLFW_KEY_PAGE_UP && action == GLFW_RELEASE) {
        camera.accelerate_front(false);
    }else if (key == GLFW_KEY_PAGE_DOWN && action == GLFW_PRESS) {
        camera.accelerate_back(true);
    }else if (key == GLFW_KEY_PAGE_DOWN && action == GLFW_RELEASE) {
        camera.accelerate_back(false);
    }else if (key == GLFW_KEY_1 && action == GLFW_PRESS) {
        set_lod(1);
        auto_lod = false;
    }else if (key == GLFW_KEY_2 && action == GLFW_PRESS) {
        set_lod(2);
        auto_lod = false;
    }else if (key == GLFW_KEY_3 && action == GLFW_PRESS) {
        set_lod(3);
        auto_lod = false;
    }else if (key == GLFW_KEY_4 && action == GLFW_PRESS) {
        set_lod(4);
        auto_lod = false;
    }else if (key == GLFW_KEY_5 && action == GLFW_PRESS) {
        set_lod(5);
        auto_lod = false;
    }else if (key == GLFW_KEY_6 && action == GLFW_PRESS) {
        set_lod(6);
        auto_lod = false;
    }else if (key == GLFW_KEY_7 && action == GLFW_PRESS) {
        set_lod(7);
        auto_lod = false;
    }else if (key == GLFW_KEY_8 && action == GLFW_PRESS) {
        set_lod(8);
        auto_lod = false;
    }else if (key == GLFW_KEY_9 && action == GLFW_PRESS) {
        set_lod(9);
        auto_lod = false;
    }else if (key == GLFW_KEY_0 && action == GLFW_PRESS) {
        auto_lod = true;
        cout << "auto LOD" << endl;
    }else if (key == GLFW_KEY_TAB && action == GLFW_PRESS) {
        view_3D = !view_3D;
        set_lod(current_lod);
        camera.pos = (*map_views.begin())->center;
        camera.pos.z += 4;
    }
}

static void scroll_callback(GLFWwindow* window,double x,double y){
    camera.focal -= y/20;
    camera.focal = std::min(camera.focal, 46.0f);
    camera.focal = std::max(camera.focal, 44.0f);
    camera.refresh_vp();
}

int main(int argc, char *argv[]) {
    if(OpenGLUtils::init_opengl() != 0) return -1;

    movables.push_back(&camera);

    for(int i=1; i<argc; i++) {
        SRTM *srtm = new SRTM(argv[i]);
        MapView *map_view = new MapView(*srtm);
        map_views.push_back(map_view);
        drawables.push_back(map_view);
    }

    glfwSetInputMode(OpenGLUtils::window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    OpenGLUtils::key_callbacks.push_back(key_callback);
    glfwSetScrollCallback(OpenGLUtils::window, scroll_callback);

    glEnable(GL_BLEND);

    set_lod(2);
    camera.pos = (*map_views.begin())->center;
    camera.pos.z += 4;

    OpenGLUtils::game_loop(world_step, render, 0.001);
    return 0;
}
