#include "srtm.h"

SRTM::SRTM(string filename) {
    w=1201;
    h=1201;
    w_deg = w * sample_deg;
    h_deg = h * sample_deg;

    int lat_sign, lng_sign;
    if(filename[0] == 'N' || filename[0] == 'n') {
        lat_sign = 1;
    }else{
        lat_sign = -1;
    }
    if(filename[3] == 'E' || filename[3] == 'e') {
        lng_sign = 1;
    }else{
        lng_sign = -1;
    }
    lat = lat_sign * (filename[1]-'0')*10 + filename[2]-'0';
    lng = lng_sign * (filename[4]-'0')*100 + (filename[5]-'0')*10 + filename[6]-'0';

    ifstream file("data/" + filename + ".hgt", ios::in|ios::binary|ios::ate);
    if(file.is_open()) {
        size = file.tellg();
        cout << "Reading file '" << filename << "' (" << size << " bytes)." << endl;
        cout << "Lat: " << lat << " Lng: " << lng << endl;
        char * temp_data = new char[size];
        file.seekg(0, ios::beg);
        file.read(temp_data, size);
        file.close();

        // transform temp_data to data
        data = new GLfloat[size/2];
        for(int i=0; i<size/2; i++) {

            data[i] = (int)(((temp_data[i*2])<<8) | ((unsigned char)temp_data[i*2+1]));
        }

        delete temp_data;
    }else{
        cout << "Cannot open file " << filename << endl;
    }
    cout << "File read." << endl;
}

SRTM::~SRTM() {
    delete[] data;
}
