#version 330 core

// Ouput data
out vec3 color;
in vec3 N;
in vec3 camera_light;

uniform vec3 camera_pos;

void main()
{
    vec3 Rcamera = 2*dot(normalize(-camera_light), normalize(N))*normalize(N) - normalize(-camera_light);


    color = vec3(1,0,0);
    color *= (dot(N, Rcamera)) * 3;
}
