#version 330 core

layout(location = 0) in vec3 pos;
layout(location = 1) in vec3 normal;

out vec3 N;
out vec3 camera_light;

uniform mat4 translation;
uniform vec3 camera_pos;

void main(){

    gl_Position = translation*vec4(pos, 1.0);
    N = normal;
    camera_light =  -camera_pos+pos;
}

