#pragma once

#include "camera.h"

class Drawable {
    public:
        virtual void draw(Camera &camera, bool *active_programs);
};
