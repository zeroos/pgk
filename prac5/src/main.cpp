#include <iostream>
#include <list>
#include <cmath>
#include <random>
#include <algorithm>
#include <string>

#include "opengl_utils.h"
#include "body.h"
#include "drawable.h"
#include "object.h"

using namespace std;

list<Drawable*> drawables;
list<Body*> movables;

Camera camera;
bool active_programs[3];
double last_xpos, last_ypos;

void world_step(double dt) {

    double xpos, ypos;
    glfwGetCursorPos(OpenGLUtils::window, &xpos, &ypos);
    glm::vec3 v = glm::vec3(0,0,-1);
    glm::vec3 u = glm::vec3(0,1,0);
    v = glm::rotateY(v, (float)-xpos/1000);
    v = glm::rotateX(v, (float)-ypos/1000);
    u = glm::rotateY(u, (float)-xpos/1000);
    u = glm::rotateX(u, (float)-ypos/1000);
    camera.look_at = v;
    camera.up = u;
    camera.perspective = true;



    for(list<Body*>::iterator i=movables.begin(); i!=movables.end(); i++) {
        (*i)->world_step((float)dt);
    }
}

void render(double alpha) {
    for(list<Drawable*>::iterator i=drawables.begin(); i!=drawables.end(); i++) {
        (*i)->draw(camera, active_programs);
    }
}

static void key_callback(GLFWwindow* window, 
        int key, int scancode, int action, int mods) {

    if (key == GLFW_KEY_LEFT && action == GLFW_PRESS) {
        camera.accelerate_left(true);
    }else if (key == GLFW_KEY_LEFT && action == GLFW_RELEASE) {
        camera.accelerate_left(false);
    }else if (key == GLFW_KEY_RIGHT && action == GLFW_PRESS) {
        camera.accelerate_right(true);
    }else if (key == GLFW_KEY_RIGHT && action == GLFW_RELEASE) {
        camera.accelerate_right(false);
    }else if (key == GLFW_KEY_UP && action == GLFW_PRESS) {
        camera.accelerate_up(true);
    }else if (key == GLFW_KEY_UP && action == GLFW_RELEASE) {
        camera.accelerate_up(false);
    }else if (key == GLFW_KEY_DOWN && action == GLFW_PRESS) {
        camera.accelerate_down(true);
    }else if (key == GLFW_KEY_DOWN && action == GLFW_RELEASE) {
        camera.accelerate_down(false);
    }else if (key == GLFW_KEY_PAGE_UP && action == GLFW_PRESS) {
        camera.accelerate_front(true);
    }else if (key == GLFW_KEY_PAGE_UP && action == GLFW_RELEASE) {
        camera.accelerate_front(false);
    }else if (key == GLFW_KEY_PAGE_DOWN && action == GLFW_PRESS) {
        camera.accelerate_back(true);
    }else if (key == GLFW_KEY_PAGE_DOWN && action == GLFW_RELEASE) {
        camera.accelerate_back(false);
    }else if (key == GLFW_KEY_1 && action == GLFW_PRESS) {
        active_programs[0] = !active_programs[0];
    }else if (key == GLFW_KEY_2 && action == GLFW_PRESS) {
        active_programs[1] = !active_programs[1];
    }else if (key == GLFW_KEY_3 && action == GLFW_PRESS) {
        active_programs[2] = !active_programs[2];
    }else if (key == GLFW_KEY_4 && action == GLFW_PRESS) {
    }else if (key == GLFW_KEY_5 && action == GLFW_PRESS) {
    }else if (key == GLFW_KEY_6 && action == GLFW_PRESS) {
    }else if (key == GLFW_KEY_7 && action == GLFW_PRESS) {
    }else if (key == GLFW_KEY_8 && action == GLFW_PRESS) {
    }else if (key == GLFW_KEY_9 && action == GLFW_PRESS) {
    }else if (key == GLFW_KEY_0 && action == GLFW_PRESS) {
    }else if (key == GLFW_KEY_TAB && action == GLFW_PRESS) {
    }
}

static void scroll_callback(GLFWwindow* window,double x,double y){
    camera.focal -= y/20;
    camera.focal = std::min(camera.focal, 46.0f);
    camera.focal = std::max(camera.focal, 44.0f);
    camera.refresh_vp();
}

int main(int argc, const char *argv[]) {
    if(OpenGLUtils::init_opengl() != 0) return -1;

    movables.push_back(&camera);
    active_programs[0] = true;
    for(int i=1; i<3; i++){
        active_programs[i] = false;
    }


    for(int i=1; i<argc; i++) {
        Object *o = new Object(argv[i]);
        drawables.push_back(o);
    }

    glfwSetInputMode(OpenGLUtils::window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    OpenGLUtils::key_callbacks.push_back(key_callback);
    glfwSetScrollCallback(OpenGLUtils::window, scroll_callback);

    glEnable(GL_BLEND);



    OpenGLUtils::game_loop(world_step, render, 0.001);
    return 0;
}
