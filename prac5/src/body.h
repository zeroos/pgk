#pragma once

#define GLM_FORCE_RADIANS

#include <glm/glm.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <iostream>

class Body {
    public:
        glm::vec3 pos;
        glm::vec3 v;

        Body();
        Body(glm::vec3 pos);
        virtual void world_step(float dt);
        virtual void collision(Body *other, float dt);
};

