#include "camera.h"

Camera::Camera() {

    pos = glm::vec3(0,0,10);



    //pos = glm::vec3(15, 80, 10);
    look_at = glm::vec3(0,0,0);
    up = glm::vec3(0, 1, 0);
    velocity = glm::vec3(0, 0, 0);
    refresh_vp();
}

void Camera::world_step(float dt) {
    float acceleration = 2;

    if(accelerating_left) {
        velocity.x -= dt*acceleration;
    }else if(accelerating_right) {
        velocity.x += dt*acceleration;
    }else if(accelerating_front) {
        velocity.z += dt*acceleration;
    }else if(accelerating_back) {
        velocity.z -= dt*acceleration;
    }else if(accelerating_up) {
        velocity.y += dt*acceleration;
    }else if(accelerating_down) {
        velocity.y -= dt*acceleration;
    }
    velocity *= dt*1000*0.999;

    pos += velocity*dt;
    refresh_vp();

    glm::vec3 t = pos+look_at;
    //std::cout << "C: "  << pos.x << " " << pos.y << " " << pos.z
    //          << " P: " << t.x   << " " << t.y   << " " << t.z << "\033[1G";
}

void Camera::refresh_vp() {
    if(perspective) {
        projection = glm::perspective(focal, 4.0f / 3.0f, 0.1f, 100.0f);
    }else{
        projection = glm::ortho((double)-pos.z, (double)pos.z, 
                                (double)-pos.z, (double)pos.z, 
                                0.0, 1000.0);
    }
    view = glm::lookAt(pos, pos+look_at, up);
    vp = projection * view;
}

void Camera::accelerate_left(bool on) {
    accelerating_left = on;
}

void Camera::accelerate_right(bool on) {
    accelerating_right = on;
}

void Camera::accelerate_front(bool on) {
    accelerating_front = on;
}

void Camera::accelerate_back(bool on) {
    accelerating_back = on;
}

void Camera::accelerate_up(bool on) {
    accelerating_up = on;
}

void Camera::accelerate_down(bool on) {
    accelerating_down = on;
}

