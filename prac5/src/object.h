#pragma once

#include <vector>

#include "drawable.h"
#include "opengl_utils.h"
#include "shader.h"
#include "objloader.h"

class Object : public Drawable {
    public:
        Object(const char * filename);
        virtual void draw(Camera &camera, bool *active_programs);
        glm::vec3 center;
    private:
        int triangles;
        GLuint vao;
        GLuint vbo;
        GLuint element_buffer;
        GLuint program1_id;
        GLuint program2_id;
        GLuint program3_id;
        GLint uni_translation;
        GLint uni_camera_position;
};
