#include "object.h"

Object::Object(const char * filename) {
    std::vector< glm::vec3 > vertices;
    std::vector< glm::vec2 > uvs;
    std::vector< glm::vec3 > normals;
    bool res = loadOBJ(filename, vertices, uvs, normals);
    triangles = vertices.size();
    cout << triangles << endl;


    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    


    program1_id = ShaderManager::load("shaders/phong.vert", 
                                      "shaders/phong.frag");

    program2_id = ShaderManager::load("shaders/silhouette.vert", 
                                      "shaders/silhouette.frag");

    program3_id = ShaderManager::load("shaders/cartoon.vert", 
                                      "shaders/cartoon.frag");

    GLuint vbo;
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, vertices.size()*sizeof(glm::vec3),
            &vertices[0], GL_STATIC_DRAW);

    GLint posAttrib = glGetAttribLocation(program1_id, "pos");
    glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(posAttrib);

    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, normals.size()*sizeof(glm::vec3),
            &normals[0], GL_STATIC_DRAW);

    GLint normalAttrib = glGetAttribLocation(program1_id, "normal");
    glVertexAttribPointer(normalAttrib, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(normalAttrib);

    uni_translation = glGetUniformLocation(program1_id, "translation");
    uni_camera_position = glGetUniformLocation(program1_id, "camera_pos");
}

void Object::draw(Camera &camera, bool *active_programs) {
    for(int i=0; i<3; i++) {
        if(!active_programs[i]) continue;
        if(i == 0){
            ShaderManager::use_program(program1_id);
        }else if(i == 1){
            ShaderManager::use_program(program2_id);
        }else if(i == 2){
            ShaderManager::use_program(program3_id);
        }

        glBindVertexArray(vao);

        glm::mat4 model;  // Changes for each model !
        glm::mat4 MVP = camera.vp * model;

        glUniformMatrix4fv(uni_translation, 1, GL_FALSE, value_ptr(MVP));
        glUniform3fv(uni_camera_position, 1, value_ptr(camera.pos));
        
        glDrawArrays(GL_TRIANGLES, 0, triangles);
    }
}
